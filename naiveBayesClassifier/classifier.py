# -*- coding: utf-8 -*-

from __future__ import division
import operator
from functools import reduce

from ExceptionNotSeen import NotSeen

class Classifier(object):
    """docstring for Classifier"""
    def __init__(self, trainedData, tokenizer):
        super(Classifier, self).__init__()
        self.data = trainedData
        self.tokenizer = tokenizer
        self.defaultProb = 0.000000001

    # ali ata bak
    def classify(self, text):

        if text == ' ' or text == '':
            return [('2', 1), ('1', self.defaultProb), ('0', self.defaultProb)]

        documentCount = self.data.getDocCount()
        classes = self.data.getClasses()


        # only unique tokens
        tokens = list(set(self.tokenizer.tokenize(text)))
        
        probsOfClasses = {}

        for className in classes:
            # we are calculating the probablity of seeing each token 
            # in the text of this class
            # P(Token_1|Class_i)
            tokensProbs = [self.getTokenProb(token, className) for token in tokens]

            # calculating the probablity of seeing the the set of tokens
            # in the text of this class
            # P(Token_1|Class_i) * P(Token_2|Class_i) * ... * P(Token_n|Class_i)
            try:
                tokenSetProb = reduce(lambda a,b: a*b, (i for i in tokensProbs if i) ) 
            except:
                tokenSetProb = 0
            
            probsOfClasses[className] = tokenSetProb * self.getPrior(className)

            # add my method
            parse = text
            if "吗" in parse or  "呢" in parse or  "怎么" in parse or  "什么" in parse\
            or "咩" in parse or "哪里" in parse or "哪个" in parse or "哪" in parse \
            or "啥" in parse or "为何" in parse or "谁" in parse or '哪种' in parse\
            or "会不会" in parse  or "难道" in parse or "诶" in parse or "如何" in parse:
                if '0' == className:
                    probsOfClasses['0'] *= 500
            elif "不" in parse or "不能" in parse or "不会" in parse or "没" in parse or "否" in parse :
                if className == '1':
                    probsOfClasses['1'] *= 1000
            else:
                if className == '2':
                    probsOfClasses['2'] *= 2000

        return sorted(probsOfClasses.items(),
            key = operator.itemgetter(1),
            reverse =True)


    def getPrior(self, className):
        return self.data.getClassDocCount(className) / self.data.getDocCount()

    def getTokenProb(self, token, className):
        # p(token|Class_i)
        classDocumentCount = self.data.getClassDocCount(className)

        # if the token is not seen in the training set, so not indexed,
        # then we return None not to include it into calculations.
        try:
            tokenFrequency = self.data.getFrequency(token, className)
        except NotSeen as e:
            return None

        # this means the token is not seen in this class but others.
        if tokenFrequency is None:
            return self.defaultProb

        probability = tokenFrequency / classDocumentCount
        return probability