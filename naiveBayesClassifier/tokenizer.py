# -*- coding: utf-8 -*-

import jieba

'''
    使用jieba来分词
'''


# tokenizer func
def tokenize(text):
    # return text.lower().split(' ')
    # text = remove_stop_word(text)
    return jieba.lcut(text, cut_all=True)


def remove_stop_word(text):
    stopWordList = [' ', '#', 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ','@','/']
    for word in stopWordList:
        text = text.strip(word)
    return text