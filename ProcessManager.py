# -*- coding: utf-8 -*-

from naiveBayesClassifier import tokenizer
from naiveBayesClassifier.trainer import Trainer
from naiveBayesClassifier.classifier import Classifier
import csv

class ProcessManager:
    '''
        To manage the process
    '''

    # sharing data
    trainAmount = 1800    # line (1,1800) for training, line (1801,2000) for testing
    trainSize = 2000
    myTrainer = Trainer(tokenizer)
    myClassifier = None
    trainSet = []

    # split array
    splitSigns = ['[', '。', '！', '？', '，', ']', '[', '!', '?', ',', ']', '//@']
    defaultSign = ','

    def __init__(self, train_data_path, train_label_path):
        self.train_data_path = train_data_path
        self.train_label_path = train_label_path

    # result
    def __init__(self, train_data_path, train_label_path, test_data_path, test_label_path):
        self.train_data_path = train_data_path
        self.train_label_path = train_label_path
        self.test_data_path = test_data_path
        self.test_label_path = test_label_path


    def set_test_data_path(self, path):
        if path == None:
            raise ProcessManager()
        self.test_data_path = path

    def set_test_label_path(self, path):
        if path == None:
            raise ProcessManager()
        self.test_label_path = path

    # load data from file to trainSet
    def load_train_set(self):
        dataOfTrain = []
        labelsOfTrain = []

        # open and read train data (train.cvs)
        with open(self.train_data_path, mode='r') as train_data:
            print("Open train data file successfully!\n")
            for line in train_data:
                line = line.lstrip('1234567890 ')   # remove line number
                line = line.strip()
                for sign in self.splitSigns:
                    line = line.replace(sign, self.defaultSign)
                arr = line.split(self.defaultSign)
                dataOfTrain.append(arr)

        # read label_train file using csv module
        train_label_file = file(self.train_label_path, 'rb')
        reader = csv.reader(train_label_file)
        for line in reader:
            labelsOfTrain.append(line)
        labelsOfTrain[0][0] = labelsOfTrain[0][0].replace('\xef\xbb\xbf', '')  # remove utf-8 bom header
        train_label_file.close()

        # combine data and label
        for i in range(0, len(dataOfTrain)):
            a, b = len(dataOfTrain[i]), len(labelsOfTrain[i])
            if cmp(a,b)  < 0:
                temp = a
            else:
                temp = b
            for j in range(0, temp):
                self.trainSet.append([dataOfTrain[i][j], labelsOfTrain[i][j]])     # data, label

    # train the classifier
    def train_classifier(self):
        for i in range(0, self.trainAmount):
            self.myTrainer.train(self.trainSet[i][0], self.trainSet[i][1])

        # When you have sufficient trained data, you are almost done and can start to use a classifier.
        self.myClassifier = Classifier(self.myTrainer.data, tokenizer)

    # write the result to the file
    def output_test_set(self):
        # open and read from test data file (data.csv)
        with open(self.test_data_path, mode='rb') as test_data:
            output_file = file(self.test_label_path, 'wb')    # open data_result.csv file to output
            writer = csv.writer(output_file)
            print("Open test file successfully!\n")

            for line in test_data:
                line = line.lstrip('1234567890 ')     # remove line number
                line = line.strip()
                for sign in self.splitSigns:
                    line = line.replace(sign, self.defaultSign)
                arr = line.split(self.defaultSign)

                row = []
                for word in arr:
                    row.append(self.myClassifier.classify(word)[0][0])
                writer.writerow(row)

            output_file.close()

    # test the accuracy of the classifier
    def test_classifier(self):
        a = 0.0
        A = 0.0

        for i in range(self.trainAmount, self.trainSize):
            result = self.myClassifier.classify(self.trainSet[i][0])[0][0]
            if result == self.trainSet[i][1]:
                a += 1
            A += 1

        # print "识别正确率: ", a/A*100, '%'
        return a / A