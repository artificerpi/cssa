#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from ProcessManager import ProcessManager


# method
def __main__():
    analysisManager = ProcessManager('./data_for_user/train.csv',
                                     './data_for_user/label_train.csv',
                                     './data_for_user/data.csv',
                                     './data_for_user/data_result.csv')

    analysisManager.load_train_set()
    analysisManager.train_classifier()
    analysisManager.output_test_set()

    result = analysisManager.test_classifier()
    print(result)

if __name__ == '__main__':
    __main__()
